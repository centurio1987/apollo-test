export const corsOptions = {
  origin: ['http://localhost:3000', 'https://studio.apollographql.com'],
  methods: ['POST', 'GET'],
  credentials: true,
};
