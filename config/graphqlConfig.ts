import { join } from 'path';

export const graphqlConfig = {
  playground: true,
  debug: false,
  typePaths: ['./**/*.graphql'],
  definitions: {
    path: join(process.cwd(), 'src/schema/generated/graphql.ts'),
    outputAs: 'class',
  },
  cors: {
    origin: ['http://localhost:3000', 'https://studio.apollographql.com'],
    methods: ['POST', 'GET', 'DELETE', 'PUT', 'OPTION'],
    credentials: true,
  },
  buildSchemaOptions: {
    dateScalarMode: 'timestamp',
  },
};
