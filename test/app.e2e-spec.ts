import { GraphQLModule } from '@nestjs/graphql';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { gql } from 'apollo-server-express';

import * as dotenv from 'dotenv';

dotenv.config();

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let graphqlModule: GraphQLModule;
  let server;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    graphqlModule = moduleFixture.get<GraphQLModule>(GraphQLModule);
    server = graphqlModule.apolloServer;
  });

  it('/hello (GET)', () => {
    return request(app.getHttpServer())
      .get('/hello')
      .expect(200)
      .expect('hello');
  });

  test('User Query Test', async () => {
    const GET_USERS = gql`
      query GetUsers {
        users {
          nickname
        }
      }
    `;

    const result = await server.executeOperation({ query: GET_USERS });
    console.log(result.data?.users);
  });
});
