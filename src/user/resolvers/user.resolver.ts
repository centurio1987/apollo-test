import { Args, Query, Resolver } from '@nestjs/graphql';
import { User } from '../../schema/generated/graphql';
import { UserService } from '../services/user.service';

@Resolver(User)
export class UserResolver {
  constructor(private readonly userService: UserService) {}
  @Query()
  async user(@Args('nickname') nickname: string) {
    return 'hello';
  }

  @Query()
  async users() {
    return await this.userService.getUsers();
  }
}
