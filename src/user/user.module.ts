import { UserDAO } from './dao/user.dao';
import { UserService } from './services/user.service';
import { Module } from '@nestjs/common';
import { UserResolver } from './resolvers/user.resolver';

@Module({
  providers: [UserResolver, UserService, UserDAO],
})
export class UserModule {}
