import { Collection } from 'fireorm';
import { User } from '../../schema/generated/graphql';

@Collection('users')
export class UserDTO extends User {
  id: string;
}
