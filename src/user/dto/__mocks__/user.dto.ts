import { User } from '../../../schema/generated/graphql';

export class MockUserDTO extends User {
  id: string;
}

const mockUserDTO = jest.fn() as jest.MockedClass<typeof User>;

mockUserDTO.mockImplementation(() => new MockUserDTO());

export default mockUserDTO;
