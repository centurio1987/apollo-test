import { UserDAO } from './../dao/user.dao';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  constructor(private readonly userDAO: UserDAO) {}

  async getUsers() {
    return await this.userDAO.getUsers();
  }
}
