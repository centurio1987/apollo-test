import { MockUserDAO } from './../dao/__mocks__/user.dao';
import { UserModule } from './../user.module';
import { UserDAO } from './../dao/user.dao';
import { UserService } from './user.service';
import { Test } from '@nestjs/testing';
import { mockServer } from 'graphql-tools';

describe('For User Service Test', () => {
  let userService: UserService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [UserModule],
    })
      .overrideProvider(UserDAO)
      .useClass(MockUserDAO)
      .compile();

    userService = await moduleRef.resolve(UserService);
  });

  test('getusers handler', async () => {
    expect(await userService.getUsers()).toBeInstanceOf(Array);
    expect((await userService.getUsers())[0].nickname).toBe('kim');
  });
});
