import { FirebaseModule } from './../../firebase/firebase.module';
import { MockFireorm } from './../../../__mocks__/fireorm';
import { Test } from '@nestjs/testing';
import UserDAO from './user.dao';

describe('User DAO Unit Test', () => {
  let userDAO: UserDAO;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [FirebaseModule],
      providers: [UserDAO],
    })
      .overrideProvider('FIREORM')
      .useClass(MockFireorm)
      .compile();

    userDAO = moduleRef.get<UserDAO>(UserDAO);
  });

  test('User CRUD TEST', async () => {
    // expect(userDAO.getUsers()).toBeCalled();

    await userDAO.getUsers();
  });
});
