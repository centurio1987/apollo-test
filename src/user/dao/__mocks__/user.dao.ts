export class MockUserDAO {
  async getUsers(): Promise<Array<any>> {
    return new Promise((resolve) => {
      resolve([
        {
          nickname: 'kim',
          description: 'for test',
          followers: ['test1', 'test2'],
          followings: ['test1', 'test2'],
          location: 'seoul',
          phoneNumber: '010-222-3333',
          profilePicture: 'hello.png',
          terms: { term1: true, term2: true },
        },
      ]);
    });
  }
}
