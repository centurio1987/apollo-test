import { Inject } from '@nestjs/common';
import { UserDTO } from '../dto/user.dto';

export class UserDAO {
  constructor(@Inject('FIREORM') private fireorm) {}

  async getUsers() {
    return await this.fireorm.getRepository(UserDTO).find();
  }
}

export default UserDAO;
