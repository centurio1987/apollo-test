
/*
 * ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export abstract class IQuery {
    abstract user(nickname?: string): User | Promise<User>;

    abstract users(): User[] | Promise<User[]>;
}

export class User {
    nickname: string;
    description?: string;
    followers?: string[];
    followings?: string[];
    location?: string;
    phoneNumber: string;
    profilePicture?: string;
    terms: Map;
}

export class Planet {
    IsPrivate: boolean;
    name: string;
    pictureName: string;
}

export class Feed {
    createdTime: Date;
    description?: string;
    isTradeOn: boolean;
    title: string;
    itemPictures?: string[];
    details?: Object[];
}

export type Map = any;
export type Object = any;
