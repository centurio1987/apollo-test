import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor<T> implements NestInterceptor {
  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<T>> {
    const logger = new Logger();
    const now = Date.now();

    return next.handle().pipe(
      tap(
        () =>
          logger.debug(
            `Class: ${context.getClass().name}, Handler: ${
              context.getHandler().name
            }, Running Time: ${Date.now() - now}ms`,
          ),
        (error) => logger.error(error),
      ),
    );
  }
}
