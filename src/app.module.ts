import { Module } from '@nestjs/common';
import { GqlModuleOptions, GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { FirebaseModule } from './firebase/firebase.module';
import { Hello } from './hello.controller';
import { UserModule } from './user/user.module';
import { graphqlConfig } from '../config/graphqlConfig';
@Module({
  imports: [
    GraphQLModule.forRootAsync({
      useFactory: async () => {
        return graphqlConfig as GqlModuleOptions;
      },
    }),
    UserModule,
    FirebaseModule,
  ],
  controllers: [Hello],
})
export class AppModule {}
