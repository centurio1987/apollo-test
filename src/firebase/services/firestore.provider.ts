import { Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import * as fireorm from 'fireorm';

@Injectable()
export class FirestoreProvider {
  private readonly db: admin.firestore.Firestore;

  constructor(@Inject('FIREBASE') admin: admin.app.App) {
    this.db = admin.firestore();
    fireorm.initialize(this.db);
  }

  getDB() {
    return this.db;
  }
}
