import { Module, Global } from '@nestjs/common';
import * as admin from 'firebase-admin';
import * as fireorm from 'fireorm';

@Global()
@Module({
  providers: [
    {
      provide: 'FIREBASE',
      useFactory: () => {
        return admin.initializeApp({
          credential: admin.credential.applicationDefault(),
        });
      },
    },
    {
      provide: 'FIRESTORE',
      useFactory: (admin) => {
        return admin.firestore();
      },
      inject: ['FIREBASE'],
    },
    {
      provide: 'FIREORM',
      useFactory: (db) => {
        fireorm.initialize(db);
        return fireorm;
      },
      inject: ['FIRESTORE'],
    },
  ],
  exports: ['FIREORM'],
})
export class FirebaseModule {}
