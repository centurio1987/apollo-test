import { Get } from '@nestjs/common';
import { Controller } from '@nestjs/common';

@Controller('hello')
export class Hello {
  @Get()
  hello() {
    return 'hello';
  }
}
