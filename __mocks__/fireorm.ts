import { UserDTO } from './../src/user/dto/user.dto';
import { User } from '../src/schema/generated/graphql';

interface MockCollection {
  find(): any;
}

export class MockFireorm {
  getRepository(classType: { new (...args: any[]): any }) {
    if (new classType() instanceof User) {
      return new (class implements MockCollection {
        async find(): Promise<User> {
          return new (class MockUser extends User {
            constructor() {
              super();
              this.nickname = 'tester';
              this.description = 'for test';
              this.followers = ['tester1', 'tester2'];
              this.followings = ['tester1', 'tester2'];
              this.location = 'test location';
              this.phoneNumber = '010-2222-3333';
              this.profilePicture = 'test.png';
              this.terms = {
                term1: true,
                term2: false,
              };
            }
          })();
        }
      })();
    }
  }
}
